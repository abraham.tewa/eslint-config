const react = require('./react');
const typescript = require('./typescript');

module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/strict-type-checked',
    'airbnb',
    'airbnb-typescript',
    'plugin:react-hooks/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:perfectionist/recommended-natural',
    './base',
  ],

  plugins: [
    'import',
    'jest',
    'react',
  ],

  rules: {
    ...react.rules,
    ...typescript.rules,
  },
};
