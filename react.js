module.exports = {
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:react-hooks/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:perfectionist/recommended-natural',
    './base',
  ],

  plugins: [
    'import',
    'jest',
    'react',
  ],

  rules: {
    'jsx-a11y/label-has-associated-control': [
      'error',
      {
        assert: 'either',
      },
    ],

    'react/jsx-filename-extension': [
      'off',
    ],

    'react/jsx-indent': [
      'error',
      2,
    ],
  },
};
