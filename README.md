# eslint-config

Personnal eslint configuration.
Use cases:
  * Javascript
  * Typescript
  * React
  * Jest

## Install

Javascript:
```sh
npm i --save-dev @abrahamtewa/eslint-config eslint-config-airbnb-base eslint-plugin-jest eslint-plugin-perfectionist
```

Typescript:
```sh
npm i --save-dev @abrahamtewa/eslint-config eslint-config-airbnb-base eslint-config-airbnb-typescript eslint-plugin-jest @typescript-eslint/eslint-plugin eslint-plugin-perfectionist
```

React
```sh
npm i --save-dev @abrahamtewa/eslint-config eslint-config-airbnb eslint-plugin-jest eslint-plugin-react eslint-plugin-react-hooks eslint-plugin-jsx-a11y eslint-plugin-perfectionist
```

Typescript + React
```sh
npm i --save-dev @abrahamtewa/eslint-config eslint-config-airbnb eslint-config-airbnb-typescript eslint-plugin-jest @typescript-eslint/eslint-plugin eslint-plugin-react eslint-plugin-react-hooks eslint-plugin-jsx-a11y eslint-plugin-perfectionist
```

## Usage

### Javascript

`.eslintrc`:
```yaml
extends:
  - "@abrahamtewa/eslint-config"
```

### Typescript
`.eslintrc`:
```yaml
parser: "@typescript-eslint/parser"

parserOptions:
  project:
    - tsconfig.json

extends:
  - "@abrahamtewa/eslint-config/typescript"
```
