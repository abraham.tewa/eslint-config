const disabledRules = [
  'no-use-before-define',
  'no-console',
  'no-underscore-dangle',
  'no-restricted-exports',
  'no-restricted-syntax',
  'import/no-named-as-default',
  'import/prefer-default-export',
];

function disableRule(rules) {
  return Object.fromEntries(
    rules.map((rule) => [rule, ['off']]),
  );
}

const importRules = {
  'import/extensions': [
    'error',
    {
      js: 'never',
      json: 'always',
      jsx: 'never',
      ts: 'never',
      tsx: 'never',
    },
  ],

  'import/no-extraneous-dependencies': [
    'error',
    {
      devDependencies: [
        '**/*.spec.js',
        '**/*.spec.jsx',
        '**/*.test.js',
        '**/*.test.jsx',

        '**/*.spec.ts',
        '**/*.spec.tsx',
        '**/*.test.ts',
        '**/*.test.tsx',

        './cypress.config.ts',
      ],
    },
  ],
};

const perfectionistRules = {
  'perfectionist/sort-classes': [
    'error',
    {
      groups: [
        'static-property',
        'property',
        'private-property',
        'constructor',
        'method',
        'private-method',
        'static-method',
        'unknown',
      ],
    },
  ],

  // Plugin: perfectionist
  'perfectionist/sort-imports': [
    'error',
    {
      groups: [
        'builtin',
        'external',
        'internal',
        'type',
        'internal-type',
        'sibling',
        'parent',
        'index',
        'sibling-type',
        'parent-type',
        'index-type',
        'object',
        'unknown',
      ],
      'newlines-between': 'ignore',
      order: 'asc',
      type: 'alphabetical',
    },
  ],

  'perfectionist/sort-union-types': [
    'off',
  ],
};

module.exports = {
  rules: {
    ...disableRule(disabledRules),

    'max-len': [
      'error',
      120,
    ],

    'no-void': [
      'error',
      {
        allowAsStatement: true,
      },
    ],

    ...importRules,
    ...perfectionistRules,
  },
};
