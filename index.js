module.exports = {
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:perfectionist/recommended-natural',
    './base',
  ],
};
