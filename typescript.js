module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/strict-type-checked',
    'airbnb-base',
    'airbnb-typescript/base',
    'plugin:perfectionist/recommended-natural',
    './base',
  ],

  rules: {
    '@typescript-eslint/consistent-type-imports': [
      'error',
    ],

    '@typescript-eslint/no-floating-promises': [
      'error',
      {
        ignoreVoid: true,
      },
    ],

    '@typescript-eslint/no-unnecessary-condition': [
      'error',
      {
        allowConstantLoopConditions: true,
      },
    ],

    '@typescript-eslint/no-use-before-define': [
      'off',
    ],

    /**
     * Why disable ?
     * This syntax is not javascript compliant. It is disable to allow
     * developpers use javascript compliant form it they want to.
     * Doc: https://typescript-eslint.io/rules/no-non-null-assertion/
     */
    '@typescript-eslint/non-nullable-type-assertion-style': [
      'off',
    ],

    '@typescript-eslint/return-await': [
      'off',
    ],

    indent: [
      'off',
    ],
  },
};
