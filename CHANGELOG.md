## [2.2.1](https://gitlab.com/abraham.tewa/eslint-config/compare/v2.2.0...v2.2.1) (2024-05-25)


### Bug Fixes

* Update typescript-eslint peer dependency version ([e7efd0c](https://gitlab.com/abraham.tewa/eslint-config/commit/e7efd0ce15645ecff5cf0edb34902435fd621d27))

# [2.2.0](https://gitlab.com/abraham.tewa/eslint-config/compare/v2.1.0...v2.2.0) (2023-10-17)


### Features

* include eslint-plugin-react-hooks and eslint-plugin-jsx-a11y ([9c7f9cc](https://gitlab.com/abraham.tewa/eslint-config/commit/9c7f9cc82b17ae9aaeb3eab5ed4493e14c507531))

# [2.1.0](https://gitlab.com/abraham.tewa/eslint-config/compare/v2.0.0...v2.1.0) (2023-10-07)


### Features

* Moving to eslint-plugin-perfectionist@2.1.0 ([f1ee795](https://gitlab.com/abraham.tewa/eslint-config/commit/f1ee7950b4b0ac43eee8495a1dbe766a58d1e205))

# [2.0.0](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.1.3...v2.0.0) (2023-08-12)


### Features

* add perfectionist rules ([f77fb1d](https://gitlab.com/abraham.tewa/eslint-config/commit/f77fb1d840fce6f11ba5c9ee2882b35d94fc3b97))


### BREAKING CHANGES

* Added rules are far more strict than before

## [1.1.3](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.1.2...v1.1.3) (2023-02-07)


### Bug Fixes

* Add missing eslint-config-airbnb-typescript peer-dependencies ([deb069b](https://gitlab.com/abraham.tewa/eslint-config/commit/deb069bc663cbbf9cc3bd5ecdf10453a8b61a891))
* Better README.md ([02dc853](https://gitlab.com/abraham.tewa/eslint-config/commit/02dc8531fe46aa187e5adca3c291245430b812db))
* using an indent 2 spaces ([df9b4e1](https://gitlab.com/abraham.tewa/eslint-config/commit/df9b4e15110a2c40d74cb155e7818c17e12dbdeb))

## [1.1.2](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.1.1...v1.1.2) (2022-09-30)


### Bug Fixes

* Include "base.js" to the package ([9144823](https://gitlab.com/abraham.tewa/eslint-config/commit/9144823c54668ca2280c024884da689824b624c2))

## [1.1.1](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.1.0...v1.1.1) (2022-09-30)


### Bug Fixes

* Include "react-typescript.js" to the package ([17c3023](https://gitlab.com/abraham.tewa/eslint-config/commit/17c3023ac9d1f37a4c0a2d744b0cdad4611b50b1))

# [1.1.0](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.0.4...v1.1.0) (2022-09-30)


### Bug Fixes

* Fix several bugs and better organization ([e0d2f7a](https://gitlab.com/abraham.tewa/eslint-config/commit/e0d2f7abf5cc51ccabc8b90f5e45d78e9130e4d5))


### Features

* creating dedicated react-typescript rules ([79d6a10](https://gitlab.com/abraham.tewa/eslint-config/commit/79d6a10f767089cca883f07c6cc3873793ef0f47))

## [1.0.4](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.0.3...v1.0.4) (2022-09-27)


### Bug Fixes

* React lint configuration ([8004735](https://gitlab.com/abraham.tewa/eslint-config/commit/80047351bae7faedca579b0e493ed55b0972b24a))

## [1.0.3](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.0.2...v1.0.3) (2022-09-27)


### Bug Fixes

* Fix no-use-before-defined rule ([df8f5ea](https://gitlab.com/abraham.tewa/eslint-config/commit/df8f5ead089778ba4489bc48434515bc22a4fb10))

## [1.0.2](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.0.1...v1.0.2) (2022-09-27)


### Bug Fixes

* **react:** issue on "jsx-a11y/label-has-associated-control" rule ([2e42f11](https://gitlab.com/abraham.tewa/eslint-config/commit/2e42f11ef3e157585fbf6d6925c122db9d36df27))

## [1.0.1](https://gitlab.com/abraham.tewa/eslint-config/compare/v1.0.0...v1.0.1) (2022-09-27)


### Bug Fixes

* republishing package ([26cc403](https://gitlab.com/abraham.tewa/eslint-config/commit/26cc4039b08496ba473afad25feb557d67d48e4c))

# 1.0.0 (2022-09-27)


### Bug Fixes

* Fix bad extends in typescript definition ([62eb6dd](https://gitlab.com/abraham.tewa/eslint-config/commit/62eb6dda364ee9a15ea66b413b1f3cf4c834571f))


### Features

* Creating configuration for base, typescript and react ([80927ac](https://gitlab.com/abraham.tewa/eslint-config/commit/80927accd900a8c944dbfc9806e5d810ae7e192c))
